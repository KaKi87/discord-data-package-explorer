import Vue from 'vue';

import App from './components/App.vue';
import messages from './data/messages';

Vue.mixin(messages);

new Vue({
    el: '#App',
    render: h => h(App)
});