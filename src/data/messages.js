export default {
    data: () => ({
        messages: {
            Import: {
                label: 'Click or drop your package file here',
                loadingLabel: 'File loading, please wait',
                errorLabel: 'File invalid or corrupted'
            }
        }
    })
};